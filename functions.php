<?php
/** functions.php
 *
 * @author		Konstantin Obenland, Gimmy
 * @package		The Bhalostrap
 * @since		1.0.0 - 05.02.2012
 */

/* function the_bhalostrap_setup() {
  load_theme_textdomain( 'the-bhalostrap', get_stylesheet_directory() . '/lang' );
}
add_action( 'after_setup_theme', 'the_bhalostrap_setup' );
*/

/* Rimuovo da bootstrap */
function remove_bootstrap_script() {
  remove_action('init','the_bootstrap_register_scripts_styles');
}
add_action( 'after_setup_theme', 'remove_bootstrap_script');

function the_bootstrap_setup() {
	global $content_width;
	
	if ( ! isset( $content_width ) ) {
		$content_width = 770;
	}
	
	load_theme_textdomain( 'the-bootstrap', get_stylesheet_directory() . '/lang' );
	
	add_theme_support( 'automatic-feed-links' );
	
	add_theme_support( 'post-thumbnails' );

	add_theme_support( 'post-formats', array(
		'aside',
		'chat',
		'link',
		'gallery',
		'status',
		'quote',
		'image',
		'video'
	) );
	
	add_theme_support( 'tha_hooks', array( 'all' ) );

	if ( version_compare( get_bloginfo( 'version' ), '3.4', '<' ) )
		// Custom Theme Options
		require_once( get_template_directory() . '/inc/theme-options.php' );
	else
		// Implement the Theme Customizer script
		require_once( get_template_directory() . '/inc/theme-customizer.php' );
	
	/**
	 * Custom template tags for this theme.
	 */
	require_once( get_template_directory() . '/inc/template-tags.php' );
	
	/**
	 * Implement the Custom Header feature
	 */
	require_once( get_template_directory() . '/inc/custom-header.php' );
	
	/**
	 * Custom Nav Menu handler for the Navbar.
	 */
	require_once( get_template_directory() . '/inc/nav-menu-walker.php' );
	
	/**
	 * Theme Hook Alliance
	 */
	require_if_theme_supports( 'tha_hooks', get_template_directory() . '/inc/tha-theme-hooks.php' );
	
	/**
	 * Including three menu (header-menu, primary and footer-menu).
	 * Primary is wrapping in a navbar containing div (wich support responsive variation)
	 * Header-menu and Footer-menu are inside pills dropdown menu
	 * 
	 * @since	1.2.2 - 07.04.2012
	 * @see		http://codex.wordpress.org/Function_Reference/register_nav_menus
	 */
	register_nav_menus( array(
		'primary'		=>	__( 'Main Navigation', 'the-bootstrap' ),
		'header-menu'  	=>	__( 'Header Menu', 'the-bootstrap' ),
		'footer-menu' 	=>	__( 'Footer Menu', 'the-bootstrap' )
	) );

	remove_action('init','the_bootstrap_register_scripts_styles');
} // the_bootstrap_setup
add_action( 'after_setup_theme', 'the_bootstrap_setup' );

/**
 * Registration of theme scripts and styles
 *
 * @author	Konstantin Obenland
 * @since	1.0.0 - 05.02.2012
 *
 * @return	void
 */
function the_bhalostrap_register_scripts_styles() {

	if ( ! is_admin() ) {
		$theme_version = _the_bootstrap_version();
		$suffix = ( defined('SCRIPT_DEBUG') AND SCRIPT_DEBUG ) ? '' : '.min';
			
		/**
		 * Scripts
		 */
		wp_register_script(
			'tw-bootstrap',
			get_template_directory_uri() . "/js/bootstrap{$suffix}.js",
			array('jquery'),
			'2.0.3',
			true
		);
		
		wp_register_script(
			'the-bootstrap',
			get_template_directory_uri() . "/js/the-bootstrap{$suffix}.js",
			array('tw-bootstrap'),
			$theme_version,
			true
		);
				
		/**
		 * Styles
		 */
		wp_register_style(
			'tw-bootstrap',
			get_stylesheet_directory_uri() . "/css/bootstrap{$suffix}.css",
			array(),
			'2.0.3'
		);
		
		wp_register_style(
			'the-bootstrap',
			get_stylesheet_directory_uri() . "/style{$suffix}.css",
			array('tw-bootstrap'),
			$theme_version
		);
	}
}
add_action( 'init', 'the_bhalostrap_register_scripts_styles' );


/* Add icon for iPhone/iPod/iPad */
function the_bootstrap_ios_icon() {
	?>
	<!-- iOS icon -->		
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/icon.png"/>
		<link rel="apple-touch-startup-image" href="img/splash.png" />
	<?php
}
add_action( 'wp_head', 'the_bootstrap_ios_icon' );


/* Filtro per esecuzione php in widget */
add_filter('widget_text','execute_php', 100);
function execute_php($html){
  if(strpos($html,"<"."?php")!==false){
    ob_start();
    eval("?".">".$html);
    $html=ob_get_contents();
    ob_end_clean();
  }
  return $html;
}

/* End of file functions.php */
