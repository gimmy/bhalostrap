CC=lessc
FLAGS= --yui-compress -x
SOURCE=style.less css/bootstrap.less
OUTPUT=$(SOURCE:.less=.min.css)
TMPFILES= *~

# output color:
#COLOR="\033["
WHITE="\033[38;1m"
GREEN="\033[32;1m"
CLOSE="\033[0m"

# prefixes:
WORK=$(GREEN)"::"$(CLOSE)
INFO=$(GREEN)" >"$(CLOSE)

all: $(OUTPUT)
	@echo -e $(INFO) $(WHITE)"Compiled"$(CLOSE)

%.min.css: %.less
	@echo -e $(WORK) $(WHITE)"Compiling" $@ $(CLOSE); $(CC) $(FLAGS) $< > $@
#	@echo -e "\t"$(GREEN)[$(CLOSE)$(WHITE)"Done"$(CLOSE)$(GREEN)]$(CLOSE)

clean:
	@echo -e $(WORK) $(WHITE)"Clean useless files"$(CLOSE)
	@rm -f $(TMPFILES) $(OUTPUT)
